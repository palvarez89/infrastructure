# Freedesktop SDK infrastructure

This repo holds:
- Terraform instructions to provision the required infrastructure
- Ansible playbooks for setting up services in that infrastructure

Full documentation of the infrastructure used by the Freedesktop SDK
project can be found in
[this repository's wiki](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/wikis/infrastructure).

## Ansible Host configuration

Currently we have 2 configured servers:

  cache.sdk.freedesktop.org >>> We host the releases here in an ostree repository, this dommain
                                is provided by ramcq at the freedesktop project.

  freedesktop-sdk-cache.codethink.co.uk >>> We host the built bst artifacts here, this domain
                                            is provided by Operations at Codethink.

The IP addresses for these servers can be found in the Hosts file, if we need a new
server the Roles in the Hosts file should be pointed to the new IP addresses. Also the managers
of each domain will have to point the DNS to the new IP addresses.

## SSL certificates
The certificates are automatically generated via the Dehydrated client/nginx configuration documented in
the Ansible scripts.

In order to obtain a VALID certificate you must ensure Dehydrated is configured with the official Lets Encrypt
URL, instead of staging(testing) URL, this can be changed via the config file, found under /etc/dehydrated/config.sh

## ostree server

The ostree server serves the release artifacts consumed by Flathub/Flatpak applications.

The infrastructure can be created with:

    # Going to the appropiate folder
    cd terraform/cache-server

    # Initialize terraform
    terraform init

    # Check changes
    terraform plan

    # Apply changes
    terraform apply


It can then be configured using Ansible as follows:

    ansible-playbook -i hosts ./releases/playbook.yml -e 'ansible_python_interpreter=/usr/bin/python3'

## BuildStream CAS cache
This is buildstreams included cache, which is an implementation of the CAS standard. We run a seperate server
which hosts all of the artifacts, which have been build/stored from the master branch.

To configure a new CAS server:

  ansible-playbook --ask-vault-pass -i hosts ./artifacts/playbook.yml -e 'ansible_python_interpreter=/usr/bin/python3'

The Vault password is currently controlled by adds68 or valentind, this is needed to decrypt the certificate files

## arm64 runners

The infrastructure can be created with:

    # Going to the appropiate folder
    cd terraform/runners

    # Initialize terraform
    terraform init

    # Check changes
    terraform plan

    # Apply changes
    terraform apply
