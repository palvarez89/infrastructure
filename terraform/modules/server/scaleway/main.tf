# Configure the Scaleway Provider
provider "scaleway" {}

data "scaleway_image" "debian_9" {
  architecture = "arm64"
  name         = "Debian Stretch"
}

resource "scaleway_server" "server" {
  count = "${var.count}"

  name  = "runner-arm64-16GB-${count.index}"
  image = "${data.scaleway_image.debian_9.id}"
  type  = "ARM64-16GB"

  dynamic_ip_required = "true"

  volume {
    size_in_gb = 150
    type       = "l_ssd"
  }
}
