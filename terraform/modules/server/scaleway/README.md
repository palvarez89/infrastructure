# Scaleway server module for Terraform

A lightweight server module for Terraform.

## Usage

module "server" {
  source = "../modules/server/scaleway"
  type   = "ARM64-16"
  count  = 2
}

See `interface.tf` for additional configurable variables.

## License

MIT
