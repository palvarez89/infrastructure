variable "do_token" {}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_volume" "cache_volume" {
  region      = "lon1"
  name        = "volume-lon1"
  size        = 500
  description = "Volume to store ostree repo(s)"
}

resource "digitalocean_droplet" "cache" {
  count = 1

  image  = "fedora-27-x64"
  name   = "artifacts-cache"
  region = "lon1"
  size   = "s-4vcpu-8gb"
  private_networking = "true"

  volume_ids = ["${digitalocean_volume.cache_volume.id}"]

  ssh_keys = ["d9:0e:f8:28:e3:99:a6:f1:82:e0:be:1c:58:09:cf:8c",
              "65:2e:5a:2b:ef:64:27:b1:e6:46:d9:e4:61:62:90:47",
              "23:2d:88:8f:2f:c3:8d:a7:9b:bb:d5:4a:1c:0c:2f:9c",
              "47:99:c9:0c:a2:ff:f8:9f:0c:68:6c:97:75:73:f7:f3"]
}

resource "digitalocean_floating_ip" "cache_floating_ip" {
  droplet_id = "${digitalocean_droplet.cache.id}"
  region     = "${digitalocean_droplet.cache.region}"
}

output "cache_floating_ip" {
  value = "${digitalocean_floating_ip.cache_floating_ip.ip_address}"
}
