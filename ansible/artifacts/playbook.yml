# Configuration for Freedesktop SDK artifacts BuildStream cache
---
- hosts: artifacts
  become: yes
  become_method: sudo
  vars:
    home_path: "/home/artifacts"
    source_path: "/root/src"
    volume_name: volume-ams3-01
    volume_path: "/mnt/{{volume_name}}"
    data_path: "{{volume_path}}/repos"
    artifacts_repo: "{{data_path}}/artifacts"
    authorized_certs: "{{home_path}}/authorized.crt"
    ssl_url: freedesktop-sdk-cache.codethink.co.uk
    public_pull_url: "https://{{ssl_url}}"
    dehydrated_domain_path: /etc/dehydrated/
    dehydrated_well_known: /var/www/dehydrated/
    certs_path: /etc/dehydrated/certs

  tasks:
  - import_tasks: install.yml

  - name: create user groups before installation
    group:
      name: ssl_certs
      state: present

  - name: create users
    user:
      name: "{{item}}"
      group: ssl_certs
    with_items:
    - artifacts

  # Disabling SELinux to prevent ansible issues
  - name: disable SELinux on subsequent boots
    selinux: state=disabled
  - name: check if SELinux is disabled
    command: getenforce
    register: result
  - name: check if we need to manually disable SELinux
    command: setenforce 0
    when: result.stdout != "Disabled"

  - name: grpc sources
    git: dest={{source_path}}/grpc repo=https://github.com/grpc/grpc version=98457b76ff43efa2c3edd203c14cd923734d5279  # v1.16.0
    register: grpc_source

  - name: buildstream installation
    command: pip3 install .
    args:
      chdir: "{{source_path}}/grpc"
    environment:
      GRPC_PYTHON_BUILD_SYSTEM_OPENSSL: "true"
      GRPC_PYTHON_BUILD_WITH_CYTHON: "1"
    when: grpc_source.changed

  - name: buildstream sources
    git: dest={{source_path}}/buildstream repo=https://gitlab.com/buildstream/buildstream version=445ee52577e8f5a9d8f79104be781f8ebee9051e  # valentindavid/cache_server_fill_up-1.2
    register: buildstream_source

  # Install BuildStream cache only
  - name: Set cache env
    shell: BST_ARTIFACTS_ONLY=1
  - name: buildstream installation
    command: pip3 install .
    args:
      chdir: "{{source_path}}/buildstream"
    when: buildstream_source.changed

  - name: create volume path
    file: path={{volume_path}} state=directory

  # This command will fail loudly if the drive has already been mounted
  # so we can simply just skip this step
  - name: ensure volume has been formatted before use
    command: "mountpoint {{volume_path}}"
    ignore_errors: yes

  - name: mount volume
    mount:
      path: "{{volume_path}}"
      src: "/dev/disk/by-id/scsi-0DO_Volume_{{volume_name}}"
      fstype: ext4
      opts: defaults,nofail,discard
      state: mounted

  - name: artifacts directory
    file: mode=2775 owner=artifacts path={{artifacts_repo}} state=directory

  - name: copy over public push certificate
    copy:
      src: ./etc/certs/public_key.yml
      dest: "{{home_path}}/client.crt"

  - name: copy over private push certificate
    copy:
      src: ./etc/certs/private_key.yml
      dest: "{{home_path}}/client.key"

  # Ensure we create a fresh authorized.crt for BuildStream, to ensure
  # the CI can push to the cache using the supplied public cert
  - name: remove old authorized certificates to avoid duplication
    file:
      path: "{{authorized_certs}}"
      state: absent
  - name: install bst-artifact push certificates
    shell: cat "{{home_path}}/client.crt" >> "{{authorized_certs}}"

  # Dehydrated is an ACME client, for managing SSL certificates, it allows
  # us to automate the process using Lets Encrypt: https://github.com/lukas2511/dehydrated
  - name: configure dehydrated
    template: src=./etc/dehydrated/config.sh dest=/etc/dehydrated/config

  - name: create our domain.txt
    template: src=./etc/dehydrated/domains.txt dest="{{dehydrated_domain_path}}/domains.txt"

  - name: create www directory
    file:
      path: /var/www/dehydrated/
      state: directory
      recurse: yes

  - name: Ensure that the existing certificate is still valid 2 weeks (1209600 seconds) from now
    openssl_certificate:
      path: "{{certs_path}}/{{ssl_url}}/cert.pem"
      provider: assertonly
      valid_in: 1209600
    ignore_errors: yes
    register: ssl_valid

  # Configure Nginx to serve over HTTP so Lets Encrypt can access the server
  # in order to retrieve the request token and validate our SSL request.
  # This is only needed if we do not find a valid SSL certificate on the
  # machine at startup.
  - name: ensure nginx is running after installation
    service: name=nginx state=started

  - name: install nginx configuration
    template: src=./etc/nginx/nginx-http.conf.in dest=/etc/nginx/nginx.conf
    when: ssl_valid is failed

  # Force restart nginx, due to an issue with notify never
  # actually restarting the service
  - name: restart nginx
    service:
      name: nginx
      state: restarted

  - name: register dehydrated
    command: dehydrated --register --accept-terms
    when: ssl_valid is failed

  - name: start dehydrated
    command: dehydrated -c
    when: ssl_valid is failed

  - name: copy over the root Lets Encrypt cert
    copy:
      src: ./etc/certs/le-root.pem
      dest: "{{certs_path}}/le-root.pem"
      owner: root

  - name: set group permissions for ssl files
    file: dest={{dehydrated_domain_path}} owner=root group=ssl_certs mode=750 recurse=yes

  - name: Create systemd service files
    template: src=./{{item}} dest=/{{item}}
    with_items:
      - etc/systemd/system/cas-serve.service
      - etc/systemd/system/cas-serve-receive.service
      - etc/systemd/system/dehydrated-renewel.service
      - etc/systemd/system/dehydrated-renewel.timer

  - name: Startup systemd units
    systemd: name={{item}} enabled=yes daemon_reload=yes state=started
    with_items:
      - cas-serve.service
      - cas-serve-receive.service
      - dehydrated-renewel.timer
